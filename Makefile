LDLIBS=-lpcap 

all: airodump

airodump: airodump.c airodump.h dumphex.h
	$(LINK.cc) $^ $(LDLIBS) -o $@ -fpermissive

clean:
	rm -f airodump *.o