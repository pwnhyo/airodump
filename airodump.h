#include <stdint.h>

#define SIZE_ETHERNET 14

/* IEEE 802.11 Radiotap header */
struct radiotap_header {
    uint8_t  it_version; 
    uint8_t  it_pad;
    uint16_t it_len;
    uint64_t it_present;
    uint8_t  it_flags;
    uint8_t  it_rate;
    uint16_t it_freq;
    uint16_t it_ch_flags;
    uint8_t it_ant_signal;
    uint8_t dummy;
    uint16_t it_rx_flags;
    uint8_t it_ant_signal2;
    uint8_t it_ant;
};

/* IEEE 802.11 Beacon frame */
struct beacon_frame {
    uint16_t frame_control;
    uint16_t duration;
    uint8_t  addr1[6];
    uint8_t  addr2[6];
    uint8_t  addr3[6];
    uint16_t sequence_control;
    //Wireless management (Fixed parameter)
    uint64_t timestamp;
    uint16_t beacon_interval;
    uint16_t capability_info;
};

struct beacon_info_element {
    uint8_t id;         /* Element ID */
    uint8_t len;        /* Length of the data field */
    uint8_t *data;     /* Variable-length data field */
};

struct output {
    uint8_t bssid[6];
    uint64_t beacons;
    uint8_t essid[32];
};