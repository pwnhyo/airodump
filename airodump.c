#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/if_ether.h>
#include "airodump.h"
#include "dumphex.h"
#include <map>
#include <array>
#include <iostream>

using Key = std::array<uint8_t, 6>; 
using namespace std;
map<Key, int> beacons_map;
map<Key, output> for_print;

// wlx705dccfbfa9c
void init()
{
  setbuf(stdout, NULL);
}

void usage()
{
  printf("syntax: airodump <interface>\n");
  printf("sample: airodump mon0\n");
}

void print_all()
{
  system("clear");
  cout << "\tBSSID\t\tbeacons\t\tESSID" << endl;

  for (auto c: for_print)
  {
    printf("  %02x:%02x:%02x:%02x:%02x:%02x", c.second.bssid[0], c.second.bssid[1], c.second.bssid[2], c.second.bssid[3], c.second.bssid[4], c.second.bssid[5]);
    printf("\t   %d\t\t", c.second.beacons);
    printf("%s", c.second.essid);
    printf("\n");

  }
}

void beacon_parser(struct pcap_pkthdr *header, const u_char *packet)
{
  
  struct radiotap_header *radiotap = (struct radiotap_header *)(packet);
  struct beacon_frame *beacon = (struct beacon_frame *)(packet + radiotap->it_len);
  
  // no beacon packet
  if (beacon->frame_control != 0x80)
  {
    return;
  }

  Key key;
  copy(begin(beacon->addr3), end(beacon->addr3), key.begin());
  
  
  if (beacons_map.find(key) != beacons_map.end()){
    beacons_map[key] += 1;
  }
  else
  {
    beacons_map[key] = 1;
    memcpy(for_print[key].bssid, beacon->addr3, 6);
  }
  for_print[key].beacons = static_cast<int>(beacons_map[key]);

  uint32_t tag_data_len = header->caplen - 24 - 24 - 12;
  uint8_t *tag_data = (uint8_t *)beacon + 24 + 12;
  uint8_t *cur = tag_data;
  while(cur < tag_data + tag_data_len)
  {
    struct beacon_info_element info;

    info.id = *cur++;
    info.len = *cur++;
    info.data = malloc(info.len);
    
    memcpy(info.data, cur, info.len);

    if (info.id == 0x00)
    {
      memcpy(for_print[key].essid, info.data, info.len);
    }

    free(info.data);
    cur += info.len;
  }

  print_all();
}

int main(int argc, char *argv[])
{
  char *interface;
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *pcap;

  if (argc != 2)
  {
      usage();
      return -1;
  }
  init();
  interface = argv[1];

  pcap = pcap_open_live(interface, BUFSIZ, 1, 1000, errbuf);
  if (pcap == NULL)
  {
    fprintf(stderr, "pcap_open_live(%s) return null - %s\n", interface, errbuf);
    return -1;
  }

  while(true)
  {
    struct pcap_pkthdr *header;
    const u_char *packet;
    int res = pcap_next_ex(pcap, &header, &packet);
    if (res == 0)
      continue;
    if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
    {
      printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
    }
    beacon_parser(header, packet);
  }

}